import csv
import gettext
import locale
import math
import numpy as np
import os
import pandas as pd
import sys
from functools import partial
from PyQt5 import sip
from PyQt5.QtGui import QIntValidator, QColor, QDoubleValidator, QIcon, QPalette
from PyQt5.QtCore import QDateTime, Qt, QTimer, QAbstractTableModel, QProcess, pyqtSignal
from PyQt5.QtWidgets import (QApplication, QButtonGroup, QCheckBox, QComboBox, QCompleter, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QMessageBox, QProgressBar, QPushButton, QRadioButton, QScrollArea, QScrollBar, QSizePolicy,
        QSlider, QSpacerItem, QSpinBox, QStyleFactory, QTableView, QTableWidget, QTabWidget, 
        QTextEdit, QVBoxLayout, QWidget)

global translation
global _
global sfBossSpeeds
global dressTranslator
global skillMods
global LOCALEPATH
global currentLanguage

global NUMBER_TABS

global MAX_TURNS
global MAX_ENTITIES
global MAX_SKILLS
global MAX_SKILL_ORDER

global STARTUP

def is_float(testStr):
    try:
        testFloat = float(testStr)
        try:
            # We do not want nan
            int(testFloat)
            return True
        except ValueError:
            return False
    except ValueError:
        return False

def setLanguage(*argv):
    global translation
    global LOCALEPATH
    global _
    global currentLanguage
    
    try:
        LOCALEPATH = os.path.join(sys._MEIPASS, 'locale')
    except Exception:
        LOCALEPATH = os.path.join(os.path.abspath("."), 'locale')
    if not argv:
        lang = os.getenv('LANG')
        if lang is None:
            lang, enc = locale.getdefaultlocale()
            if lang:
                os.environ['LANG'] = lang
        if 'zh' in lang:
            currentLanguage = 1;
        else:
            currentLanguage = 0;
        try:
            translation = gettext.translation('labels', LOCALEPATH)
            _ = translation.gettext
        except:
            _ = lambda x: x
    else:
        if argv[0] == 'zh':
            currentLanguage = 1;
        else:
            currentLanguage = 0;
        os.environ['LANG'] = argv[0]
        try:
            translation = gettext.translation('labels', LOCALEPATH)
            _ = translation.gettext
        except:
            _ = lambda x: x

class checkBoxLineEdit(QLineEdit):
    #doubleClicked = pyqtSignal()
    clicked = pyqtSignal()
    #def __init__(self):
        #QPushButton.__init__(self, *args, **kwargs)
        #self.timer = QTimer()
        #self.timer.setSingleShot(True)
        #self.timer.timeout.connect(self.clicked.emit)
        #super().clicked.connect(self.checkDoubleClick)
        #pass
    
    #def checkDoubleClick(self):
        #if self.timer.isActive():
        #    self.doubleClicked.emit()
        #    self.timer.stop()
        #else:
        #    self.timer.start(250)
    def mouseReleaseEvent(self, QMouseEvent):
        if QMouseEvent.button() == Qt.LeftButton:
            self.clicked.emit()

# Table Model
class TableModel(QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self.header_labels = []
        for i in range(len(data[0])):
            self.header_labels.append(data[0][i])
        self._data = []
        for i in range(1,len(data)):
            self._data.append(data[i])

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._data[0])
        
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self.header_labels[section]
        return QAbstractTableModel.headerData(self, section, orientation, role)

# Damage Calculation
class Mults:
    def __init__(self, multMods):
        # Initialize mods for mults
        self.numTargets = multMods[0]
        self.targetDEF = multMods[1]
        self.noCritMult = multMods[2]
        self.numHits = multMods[3]
        self.AOE = multMods[4]
        self.skillMods = multMods[5]
        self.skLvBonus = multMods[6]
        self.statBuffs = multMods[7]                # ATK,HP,DEF,SPD
        self.critBuff = multMods[8]
        self.defDown = multMods[9]
        self.burn = multMods[10]
        self.freeze = multMods[11]
        self.sfBonus = multMods[12]
        self.forcedCrit = multMods[13]
        self.forcedHeavy = multMods[14]
        self.ignoreDEF = multMods[15]
        self.elemAdv = multMods[16]
        self.otherBonus = multMods[17]
        self.eventBonus = multMods[18]
        
        # Calculate Stat Buffs
        self.statBuffs[0] = self.statBuffs[0] * 0.5  # ATK Buff
        self.statBuffs[2] = self.statBuffs[2] * 0.7  # DEF Buff
        self.statBuffs[3] = self.statBuffs[3] * 0.3  # SPD Buff
        
        # Skill Bonus is in %
        self.skLvBonus = self.skLvBonus/100
        self.otherBonus = self.otherBonus/100
        
        # Calculate Mult Inputs
        self.baseMult = 0.0  
        self.index = 0
        self.ccBonus = 0.0
        self.cdBonus = 0.0
        self.critHeavyMod = 0.0
        
        
        # Mult Lists
        self.baseMultList = []
        self.mult = []
        self.ccMult = []
        self.cdMult = []
        self.cccdMult = []
        
        for i in range(4):
            self.calculateBaseMult()
            self.baseMultList.append(self.baseMult)
            self.ccBonus = 0.0
            self.cdBonus = 0.0
            self.calculateCHMod()
            self.mult.append(self.baseMultList[i] * self.critHeavyMod)
            self.ccBonus = 0.25
            self.cdBonus = 0.0
            self.calculateCHMod()
            self.ccMult.append(self.baseMultList[i] * self.critHeavyMod)
            self.ccBonus = 0.0
            self.cdBonus = 0.25
            self.calculateCHMod()
            self.cdMult.append(self.baseMultList[i] * self.critHeavyMod)
            self.ccBonus = 0.25
            self.cdBonus = 0.25
            self.calculateCHMod()
            self.cccdMult.append(self.baseMultList[i] * self.critHeavyMod)
            self.index = self.index + 1
        #print(self.baseMultList)
        for j in range(4):
            if not (is_float(self.mult[j])):
                self.mult[j] = 0.0;
            if not (is_float(self.ccMult[j])):
                self.ccMult[j] = 0.0;
            if not (is_float(self.cdMult[j])):
                self.cdMult[j] = 0.0;
            if not (is_float(self.cccdMult[j])):
                self.cccdMult[j] = 0.0;
        
    def calculateBaseMult(self):
        stat = self.index
        if (is_float(self.ignoreDEF) == True):
            defense = self.ignoreDEF + (1-self.ignoreDEF)*((1-self.defDown)/(1+self.targetDEF/750) + self.defDown/(1+0.3*self.targetDEF/750))
        else:
            defense = 0.0
        self.baseMult = self.numHits * self.skillMods[stat] * (1 + self.skLvBonus) * (1 + self.statBuffs[stat]) * \
                   (1 + 0.3*self.burn + 0.3*self.freeze + 0.5*self.sfBonus + self.otherBonus + self.eventBonus) * \
                   (1 + self.AOE*(self.numTargets-1)) * defense
    
    def calculateCHMod(self):
        critChance = (1-self.forcedHeavy)*(self.forcedCrit + (1-self.forcedCrit)* \
                     (1-self.noCritMult*(1-(0.3 + 0.3*self.critBuff + 0.15*self.elemAdv + self.ccBonus))))
        heavyChance = self.forcedHeavy + 0.3*(1-self.forcedHeavy)*(1-critChance)*self.elemAdv
        self.critHeavyMod = 1 + (0.5+self.cdBonus)*critChance + 0.3*heavyChance


# GUI
class WidgetGallery(QDialog):
    def __init__(self, parent=None):
        super(WidgetGallery, self).__init__(parent)
        global LOCALEPATH
        global MAX_TURNS
        global MAX_ENTITIES
        global MAX_SKILLS
        global MAX_SKILL_ORDER
        global NUMBER_TABS
        global STARTUP

        self.originalPalette = QApplication.palette()
        QApplication.setPalette(self.originalPalette)
        QApplication.setStyle(QStyleFactory.create("Fusion"))
        
        # Setting Limit Values
        MAX_ENTITIES = 5            # 4 Dress, 1 Enemy
        MAX_TURNS = 41              # Max number of turns for simulating (+1)
        MAX_SKILLS = 3              # Max number of skills a dress can have
        MAX_SKILL_ORDER = 40        # Max number of turns each dress can take
        NUMBER_TABS = 8             # Number of tabs for the GUI
        STARTUP = True              # Initializing GUI
        
        # Language
        self.languageList = next(os.walk(LOCALEPATH))[1]
        self.languageListTranslated = []
        for i in self.languageList:
            self.languageListTranslated.append(_(i))
        # Map for Labels:
        # 0: Main Tabs
        # 1: Team + Turns
        # 2: Turn Simulator
        #
        self.labelStringArray = []

        # Load Sheets
        try:
            multsCalc = pd.read_excel('Copy of NYA Mults Calculator.xlsx', sheet_name = None)
        except:
            for file in os.listdir('.'):
                if file.endswith('.xlsx'):
                    multsCalc = pd.read_excel(file, sheet_name = None)

        for i in multsCalc.keys():
            if 'SF Boss Speeds' in i:
                self.sfBossSpeeds = multsCalc[i]
            if 'Dress Translator' in i:
                self.dressTranslator = multsCalc[i]
            if 'Skill mods' in i:
                self.skillMods = multsCalc[i]
                
        self.dressList = self.dressTranslator.iloc[:,[1, 2]]
        
        # Optimizer CSVs
        # Initialize results CSV in case it has never been run beforee
        self.resultsCSV = []
        for i in range(21):
            tempRow = []
            for j in range(19):
                if (i == 0):
                    tempRow.append('')
                else:
                    tempRow.append('0')
            self.resultsCSV.append(tempRow)
        
        for file in os.listdir('.'):
            if (file == 'mults.csv'):
                self.multsCSV = pd.read_csv(file)
            if (file == 'add_stats.csv'):
                self.addStatsCSV = pd.read_csv(file)
            if (file == 'min_stats.csv'):
                self.minStatsCSV = pd.read_csv(file)
            if (file == 'max_stats.csv'):
                self.maxStatsCSV = pd.read_csv(file)
            if (file == 'order.txt'):
                self.orderTXT = pd.read_csv(file)
            if (file == 'db.csv'):
                self.dbCSV = pd.read_csv(file)
            if (file == 'dresses.csv'):
                self.dressesCSV = pd.read_csv(file)
            if (file == 'orbs.csv'):
                self.orbsCSV = pd.read_csv(file)
            if (file == 'results.csv'):
                self.resultsCSV = pd.read_csv(file)
            if (file == 'stats.csv'):
                self.statsCSV = pd.read_csv(file)
            if (file == 'optimize.exe'):
                self.optimizePath = file
                self.optimizerDir = os.getcwd()
            if os.path.isdir(file):
                for subFile in os.listdir('./'+file):
                    filepath = os.path.join(file + '/' + subFile)
                    if (subFile == 'mults.csv'):
                        self.multsCSV = pd.read_csv(filepath)
                    if (subFile == 'add_stats.csv'):
                        self.addStatsCSV = pd.read_csv(filepath)
                    if (subFile == 'min_stats.csv'):
                        self.minStatsCSV = pd.read_csv(filepath)
                    if (subFile == 'max_stats.csv'):
                        self.maxStatsCSV = pd.read_csv(filepath)
                    if (subFile == 'order.txt'):
                        self.orderTXT = pd.read_csv(filepath)
                    if (subFile == 'db.csv'):
                        self.dbCSV = pd.read_csv(filepath)
                    if (subFile == 'dresses.csv'):
                        self.dressesCSV = pd.read_csv(filepath)
                    if (subFile == 'orbs.csv'):
                        self.orbsCSV = pd.read_csv(filepath)
                    if (subFile == 'results.csv'):
                        self.resultsCSV = pd.read_csv(filepath)
                    if (subFile == 'stats.csv'):
                        self.statsCSV = pd.read_csv(filepath)
                    if (subFile == 'optimize.exe'):
                        self.optimizePath = filepath
                        self.optimizerDir = file
                        
        # Change Dataframes to Lists
        self.multsCSV = self.multsCSV.T.reset_index().values.T.tolist()
        self.minStatsCSV = self.minStatsCSV.T.reset_index().values.T.tolist()
        self.maxStatsCSV = self.maxStatsCSV.T.reset_index().values.T.tolist()
        self.addStatsCSV = self.addStatsCSV.T.reset_index().values.T.tolist()
        self.orderTXT = self.orderTXT.T.reset_index().values.T.tolist()
        self.dressesCSV = self.dressesCSV.T.reset_index().values.T.tolist()
        self.orbsCSV = self.orbsCSV.T.reset_index().values.T.tolist()
        try:
            self.resultsCSV = self.resultsCSV.T.reset_index().values.T.tolist()
        except:
            pass
        try:
            self.statsCSV = self.statsCSV.T.reset_index().values.T.tolist()
        except:
            self.statsCSV = []
        
        # Widget Color Palette List
        self.widgetColors = []
        colorCodes = [[230,205,255], [205,225,255], [215,255,205], [255,180,180],[]]
        for i in range(MAX_ENTITIES):
            try:
                tempPalette = QPalette()
                tempPalette.setColor(QPalette.Window, QColor(colorCodes[i][0],colorCodes[i][1],colorCodes[i][2]))
                tempPalette.setColor(QPalette.Base, QColor(colorCodes[i][0],colorCodes[i][1],colorCodes[i][2],50))
                tempPalette.setColor(QPalette.Button, QColor(colorCodes[i][0],colorCodes[i][1],colorCodes[i][2],5))
                self.widgetColors.append(tempPalette)
            except:
                self.widgetColors.append(self.palette())
        
        # S4 Bonuses
        self.s4BonusList = [_('ATK'), _('HP'), _('DEF')]

        # Change Application Label and Icon
        self.setWindowIcon(QIcon('nya.png'))
        self.setWindowTitle(_("MGCM Optimizer"))
        self.setWindowFlags(Qt.WindowMinimizeButtonHint|Qt.WindowMaximizeButtonHint|Qt.WindowCloseButtonHint)
        
        # Initialize Widget Arrays
        # Main Tabs
        self.mainTabs = []
        # Team Dresses
        self.teamDressLabelObjects = []
        self.teamDressRows = []
        self.currentDresses = [False, False, False, False, True]
        self.skillOrderLabelWidgets = []
        self.defaultSpeeds = ['235', '234', '233', '232', '200']
        # Turn Simulator Lists
        self.turnSimRows = []
        self.turnOrder = []
        
        # Mults
        self.multsArray = []
        self.multsWidgets = []
        self.unroundedMults = np.zeros((len(self.multsCSV), len(self.multsCSV[0])))
        self.unroundedMults = self.unroundedMults.tolist()
        
        # CSV Widgets
        self.minStatsWidgets = []
        self.maxStatsWidgets = []
        self.addStatsWidgets = []
        self.orderWidgets = []
        self.resultsWidgets = []
        self.saveCSVPushbuttons = []
        
        # Creating Main Tab
        self.labelStringArray.append(["&Team + Turns", "&Optimizer Tuning", "&Mults + Optimization", "&Results", "&Statistics", "&Extras", "&Data", "&Settings"])
        self.createMainTabWidget()

        # Creating Main Window
        self.mainLayout = QHBoxLayout()
        self.mainLayout.addWidget(self.MainTabWidget, 0)
        self.setMinimumHeight(480)
        self.setLayout(self.mainLayout)
        
        # Problem with calculating mults after setting all default values early. So we do it at the end of init.
        self.targetSpeedLineEdit.setText(self.defaultSpeeds[MAX_ENTITIES-1])
        self.simulateTurns()
        
        # Solves issue where user opens app and wants to run optimizer immediately with a previously modified mults.csv
        STARTUP = False
    
    # Main Window
    def createMainTabWidget(self):
        global NUMBER_TABS
        self.MainTabWidget = QTabWidget()
        self.MainTabWidget.setSizePolicy(QSizePolicy.Preferred,
                QSizePolicy.Ignored)
        
        for i in range(NUMBER_TABS):
            tempTab = QWidget()
            self.mainTabs.append(tempTab)
            
        # Tab 1 - Team + Turns
        self.createTab1Widget()
        
        # Tab 2 - Optimizer Tuning
        self.createTab2Widget()
        
        # Tab 3 - Optimization
        self.createTab3Widget()
        
        # Tab 4 - Results
        self.createTab4Widget()
        
        # Tab 5 - Statistics
        #self.createTab5Widget()
        
        # Tab 6 - Extras
        self.createTab6Widget()
        
        # Tab 7 - Data
        self.createTab7Widget()
        
        # Tab 8 - Settings
        self.createTab8Widget()
        
        for i in range(NUMBER_TABS):
            self.MainTabWidget.addTab(self.mainTabs[i], _(self.labelStringArray[0][i]))
        
    # Tab 1 - Team and Turns
    def createTab1Widget(self):
        global MAX_TURNS
        global MAX_ENTITIES
        global MAX_SKILLS
        global MAX_SKILL_ORDER
        labelCount = 15
        # General
        topBox = QWidget()
        bottomBox = QWidget()
        turnSimPushButtonBox = QWidget()
        
        # Labels
        self.labelStringArray.append(["Num. Targets", "# Turns", "Target DEF", "No-Crit Mult", "Target Speed", "Team", \
                                      "Skill Level Bonuses", "S4 Bonus (%)", "Dresses", "Skill 1 (%)", "Skill 2 (%)", \
                                      "Skill 3 (%)", "Speed", "Skill Order", "Event Bonus (%)"])
        labelTeamArray = [[0, 0], [0, 8], [1, 0], [2, 0], [2, 8], [3, 0], [3, 3],\
                          [3, 6], [4, 1], [4, 3], [4, 4], [4, 5], [4, 8], [4, 9], [4, 7]]
        for i in range(labelCount):
            tempLabel = QLabel(_(self.labelStringArray[1][i]))
            if (i<5):
                tempLabel.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
            self.teamDressLabelObjects.append(tempLabel)
        
        self.targetsLineEdit = QLineEdit()
        self.targetDefLineEdit = QLineEdit()
        self.noCritMultLineEdit = QLineEdit()
        self.targetSpeedLineEdit = QLineEdit()
        self.numberTurnsLineEdit = QLineEdit()
        self.targetsLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
        self.targetDefLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
        self.noCritMultLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
        self.targetSpeedLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
        self.numberTurnsLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
        self.targetsLineEdit.setText('1')
        self.targetDefLineEdit.setText('1000')
        self.noCritMultLineEdit.setText('1')
        #self.targetSpeedLineEdit.setText(self.defaultSpeeds[MAX_ENTITIES-1])
        self.numberTurnsLineEdit.setText('20')
        self.targetsLineEdit.setValidator(QIntValidator(0,9))
        self.targetDefLineEdit.setValidator(QDoubleValidator(0,9999,2))
        self.noCritMultLineEdit.setValidator(QDoubleValidator(0,1,5))
        self.targetSpeedLineEdit.setValidator(QIntValidator(0,999))
        self.numberTurnsLineEdit.setValidator(QIntValidator(0,99))
        self.targetSpeedLineEdit.textEdited.connect(self.simulateTurns)
        self.numberTurnsLineEdit.textEdited.connect(self.simulateTurns)
        calcMultsCall = partial(self.recalculateMults, "all")
        self.targetsLineEdit.textEdited.connect(calcMultsCall)
        self.targetDefLineEdit.textEdited.connect(calcMultsCall)
        self.noCritMultLineEdit.textEdited.connect(calcMultsCall)
        
        self.teamLineEdits = []
        self.teamLineEdits.append(self.targetsLineEdit)
        self.teamLineEdits.append(self.targetDefLineEdit)
        self.teamLineEdits.append(self.noCritMultLineEdit)
        self.teamLineEdits.append(self.targetSpeedLineEdit)
        self.teamLineEdits.append(self.numberTurnsLineEdit)

        # S4 Bonuses
        self.s4BonusLabel = QLabel(_("S4 Bonus (%)"))
        self.s4BonusComboBox = QComboBox()
        self.s4BonusComboBox.currentTextChanged.connect(self.sk4Changed)
        self.s4BonusComboBox.addItems(self.s4BonusList)
        # Unit Skill Order
        skillNumbers = ['1', '2', '3']
        
        # Team Selection Widget Array
        for i in range(MAX_ENTITIES-1):
            tempRow = []
            # Dress
            dressComboBox = QComboBox()
            dressComboBox.addItems(self.dressList.iloc[:,currentLanguage].tolist())
            dressComboBox.setEditable(True)
            dressComboBox.setInsertPolicy(QComboBox.NoInsert)
            dressComboBox.completer().setCompletionMode(QCompleter.PopupCompletion)
            dressComboBox.setMaximumWidth(200)
            dressComboBox.setMinimumWidth(200)
            dressComboBox.completer().setFilterMode(Qt.MatchContains)
            dressComboBox.setCurrentText("")
            dressCall = partial(self.dressChangedCallback,i)
            dressComboBox.currentTextChanged.connect(dressCall)
            dressComboBox.currentIndexChanged.connect(dressCall)
            try: 
                dressComboBox.setPalette(self.widgetColors[i])
            except:
                print(self.widgetColors[i])
            tempRow.append(dressComboBox)
            # Blank Space so Dress Box has more room
            tempRow.append(None)
            # Skills
            for j in range(MAX_SKILLS):
                skillLineEdit = QLineEdit()
                skillLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
                skillLineEdit.setValidator(QIntValidator(0,999))
                tempRow.append(skillLineEdit)
            # s4 Bonus
            s4BonusLineEdit = QLineEdit()
            s4BonusLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
            s4BonusLineEdit.setValidator(QIntValidator(0,999))
            tempRow.append(s4BonusLineEdit)
            # Speed
            speedLineEdit = QLineEdit()
            speedLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
            speedLineEdit.setValidator(QIntValidator(0,999))
            try:
                speedLineEdit.setText(str(self.statsCSV[i+1][4]))
                
            except:
                speedLineEdit.setText(self.defaultSpeeds[i])
            speedLineEdit.textEdited.connect(self.simulateTurns)
            tempRow.append(speedLineEdit)
            # Skill Order
            for j in range(MAX_SKILL_ORDER):
                skillOrderComboBox = QComboBox()
                skillOrderComboBox.addItems(skillNumbers)
                skillOrderComboBox.currentTextChanged.connect(self.simulateTurns)
                skillOrderComboBox.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
                skillOrderComboBox.setVisible(False)
                try: 
                    skillOrderComboBox.setPalette(self.widgetColors[i])
                except:
                    print(self.widgetColors[j])
                tempRow.append(skillOrderComboBox)
            eventBonusLineEdit = QLineEdit()
            eventBonusLineEdit.setSizePolicy(QSizePolicy.Fixed,QSizePolicy.Fixed)
            eventBonusLineEdit.textChanged.connect(self.simulateTurns)
            eventBonusLineEdit.setValidator(QIntValidator(0,999))
            tempRow.append(eventBonusLineEdit)
            
            # Append Widget Row to Array
            self.teamDressRows.append(tempRow)
            
        scrollLayout = QGridLayout()
        
        scrollWidget = QWidget()
        scrollWidget.setLayout(scrollLayout)
        
        scrollArea = QScrollArea()
        scrollArea.setWidgetResizable(True)
        scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        scrollArea.setWidget(scrollWidget)
        scrollLayout.setAlignment(Qt.AlignTop)
        
        # Turns
        # General
        self.turnSimLabel = QLabel(_("Turn Simulator"))
        self.turnSimButtonLabel = QLabel(_("Turn Simulator Options"))
        self.turnSimResetPushButton = QPushButton(_("Reset Turn Simulator Selections"))
        self.turnSimResetPushButton.clicked.connect(self.resetTurnSimSelections)
        
        self.turnSimSavePushButton = QPushButton(_("Save Turn Simulator Selections"))
        self.turnSimSavePushButton.clicked.connect(self.saveTurnSimSelections)
        
        self.turnSimLoadPushButton = QPushButton(_("Load Turn Simulator Selections"))
        self.turnSimLoadPushButton.clicked.connect(self.loadTurnSimSelections)
        
        self.turnSimScrollLayout = QGridLayout()
        
        self.turnSimScrollWidget = QWidget()
        self.turnSimScrollWidget.setLayout(self.turnSimScrollLayout)
        
        self.turnSimScrollArea = QScrollArea()
        self.turnSimScrollArea.setWidgetResizable(True)
        self.turnSimScrollArea.setWidget(self.turnSimScrollWidget)
        
        # Turn Rows
        # General
        speedOptions = [" ",u"\u2191", u"\u2193"]
        verticalSpacer = QSpacerItem(40, 20,  QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.turnSimScrollLayout.addItem(verticalSpacer, 0, 6, Qt.AlignTop)
        # Labels
        self.createTurnSimLabels()
        
        # Widget Rows
        for i in range(MAX_TURNS-1):
            tempRow = []
            # Turn Label
            turnLabel = QLabel(str(i+1))
            turnLabel.setMinimumWidth(14)
            turnLabel.setMaximumWidth(14)
            tempRow.append(turnLabel)
            # Speed Up/Down
            for speed in range(MAX_ENTITIES):
                spdComboBox = QComboBox()
                spdComboBox.addItems(speedOptions)
                spdComboBox.setSizePolicy(QSizePolicy.Maximum,QSizePolicy.Maximum)
                spdComboBox.activated.connect(self.simulateTurns)
                try: 
                    spdComboBox.setPalette(self.widgetColors[speed])
                except:
                    print(self.widgetColors[speed])
                tempRow.append(spdComboBox)
            # Gauge Up/Down
            for gauge in range(MAX_ENTITIES):
                gaugeLineEdit = QLineEdit()
                gaugeLineEdit.setMinimumWidth(40)
                gaugeLineEdit.setMaximumWidth(40)
                gaugeLineEdit.setSizePolicy(QSizePolicy.Maximum,QSizePolicy.Maximum)
                gaugeLineEdit.setValidator(QIntValidator(-999,999))
                gaugeLineEdit.textEdited.connect(self.simulateTurns)
                try: 
                    gaugeLineEdit.setPalette(self.widgetColors[gauge])
                except:
                    print(self.widgetColors[gauge])
                tempRow.append(gaugeLineEdit)
            dressTurnLabel = QLabel("0")
            dressTurnLabel.setMaximumWidth(16)
            dressTurnLabel.setAlignment(Qt.AlignCenter)
            tempRow.append(dressTurnLabel)
            dressLineEdit = QLineEdit()
            dressLineEdit.setReadOnly(True)
            dressLineEdit.setMinimumWidth(150)
            dressLineEdit.setMaximumWidth(150)
            dressLineEdit.setText("")
            tempRow.append(dressLineEdit)
            dressSkLabel = QLabel("0")
            dressSkLabel.setMaximumWidth(16)
            dressSkLabel.setAlignment(Qt.AlignCenter)
            tempRow.append(dressSkLabel)
            for boxes in range(12):
                checkBox = checkBoxLineEdit()
                checkBox.setValidator(QDoubleValidator(0,1,3))
                checkBox.setMinimumWidth(80)
                checkBox.setMaximumWidth(80)
                clickCall = partial(self.setCheckBox, i, boxes)
                checkBox.clicked.connect(clickCall)
                calcMultsCall = partial(self.recalculateMults, str(i))
                checkBox.textEdited.connect(calcMultsCall)
                tempRow.append(checkBox)
            otherSkBonusLineEdit = QLineEdit()
            otherSkBonusLineEdit.setValidator(QIntValidator(0,999))
            otherSkBonusLineEdit.setMinimumWidth(80)
            otherSkBonusLineEdit.setMaximumWidth(80)
            calcMultsCall = partial(self.recalculateMults, str(i))
            otherSkBonusLineEdit.textEdited.connect(calcMultsCall)
            tempRow.append(otherSkBonusLineEdit)
            for mults in range(4):
                multsLabel = QLabel("0.0")
                if (mults != 3):
                    multsLabel.setMinimumWidth(50)
                    multsLabel.setMaximumWidth(50)
                tempRow.append(multsLabel)
            self.turnSimRows.append(tempRow)
        
        for i in range(MAX_TURNS):
            for j in range(len(self.turnSimRows[0])):
                if (self.turnSimRows[i][j] != None):
                    if (i == 0):
                        if ((j == 3) or (j == 8)):
                            self.turnSimScrollLayout.addWidget(self.turnSimRows[i][j],i,j-2,1,5)
                        elif (j > 10):
                            self.turnSimScrollLayout.addWidget(self.turnSimRows[i][j],i,j)
                    else:
                        self.turnSimRows[i][j].setVisible(False)
                        self.turnSimScrollLayout.addWidget(self.turnSimRows[i][j],i,j)
        
        # Layout
        # Top Box
        topLayout = QGridLayout()
        # Top Line Edits
        topLayout.addWidget(self.targetsLineEdit, 0, 1)
        topLayout.addWidget(self.targetDefLineEdit, 1, 1)
        topLayout.addWidget(self.numberTurnsLineEdit, 1, 8)
        topLayout.addWidget(self.noCritMultLineEdit, 2, 1)
        topLayout.addWidget(self.targetSpeedLineEdit, 3, 8)
        topLayout.addWidget(self.s4BonusComboBox, 4, 6)
        # Top Labels
        for i in range(labelCount):
            if (i == 6):
                topLayout.addWidget(self.teamDressLabelObjects[i],labelTeamArray[i][0], labelTeamArray[i][1],1,3)
            elif (i == 13):
                scrollLayout.addWidget(self.teamDressLabelObjects[i],0,0,1,4)
            else:
                topLayout.addWidget(self.teamDressLabelObjects[i],labelTeamArray[i][0], labelTeamArray[i][1])
        # Top Team Dresses
        for i in range(MAX_SKILL_ORDER):
            turnNumber = QLabel(str(i+1))
            turnNumber.setVisible(False)
            self.skillOrderLabelWidgets.append(turnNumber)
            scrollLayout.addWidget(self.skillOrderLabelWidgets[i],1,i)
        for i in range(len(self.teamDressRows)):
            for j in range(len(self.teamDressRows[i])):
                if (self.teamDressRows[i][j] != None):
                    if (j == 0):
                        topLayout.addWidget(self.teamDressRows[i][j],i+5,j+1,1,2)
                    elif (j < 6):
                        topLayout.addWidget(self.teamDressRows[i][j],i+5,j+1)
                    elif (j == 6):
                        topLayout.addWidget(self.teamDressRows[i][j],i+5,j+2)
                    elif (j == (len(self.teamDressRows[i])-1)):    
                        topLayout.addWidget(self.teamDressRows[i][j],i+5,7)
                    else:
                        if ((i == 0) and (j == 7)):
                            topLayout.addWidget(scrollArea,i+3,j+2,8,6)
                        scrollLayout.addWidget(self.teamDressRows[i][j],i+2,j-7)
        
        # This is to have dress combobox not stretch
        tempLabel = QLabel('')
        tempLabel.setMinimumWidth(90)
        tempLabel.setMaximumWidth(90)
        topLayout.addWidget(tempLabel,4,2)
        
        topBox.setLayout(topLayout)
        #Bottom Box
        bottomLayout = QGridLayout()
        bottomLayout.addWidget(self.turnSimLabel,0,0)
        bottomLayout.addWidget(self.turnSimScrollArea,1,0)
        bottomLayout.setAlignment(Qt.AlignLeft)
        bottomBox.setLayout(bottomLayout)
        
        #Turn Sim PushButton Box
        turnSimPushButtonLayout = QGridLayout()
        turnSimPushButtonLayout.addWidget(self.turnSimButtonLabel,0,0)
        turnSimPushButtonLayout.addWidget(self.turnSimSavePushButton,1,0)
        turnSimPushButtonLayout.addWidget(self.turnSimLoadPushButton,1,1)
        turnSimPushButtonLayout.addWidget(self.turnSimResetPushButton,1,2)
        turnSimPushButtonBox.setLayout(turnSimPushButtonLayout)
        
        # Tab
        tab1Layout = QGridLayout()
        tab1Layout.addWidget(topBox,0,0)
        tab1Layout.addWidget(bottomBox,1,0)
        tab1Layout.addWidget(turnSimPushButtonBox,2,0)
        self.mainTabs[0].setLayout(tab1Layout)
    
    # Tab 2 - Optimizer Tuning
    def createTab2Widget(self):
        self.tab2LabelObjects = []
        self.labelStringArray.append(["Min Stats", "Max Stats", "Add Stats", "Turn Order"])
        for i in range(len(self.labelStringArray[3])):
            self.tab2LabelObjects.append(QLabel(_(self.labelStringArray[3][i])))
        
        # Scroll Areas 
        self.csvScrollAreas = []
        self.csvScrollLayout = []
        for i in range(4):
            scrollLayout = QGridLayout()
        
            scrollWidget = QWidget()
            scrollWidget.setLayout(scrollLayout)
            
            scrollArea = QScrollArea()
            scrollArea.setWidgetResizable(True)
            scrollArea.setWidget(scrollWidget)
            scrollLayout.setAlignment(Qt.AlignTop)
            
            self.csvScrollAreas.append(scrollArea)
            self.csvScrollLayout.append(scrollLayout)
            
        # Min/Max Stats
        for i in range(len(self.maxStatsCSV)):
            tempMinRow = []
            tempMaxRow = []
            for j in range(len(self.maxStatsCSV[i])):
                if ((i == 0) or (j == 0)):
                    tempMinRow.append(QLabel(str(self.minStatsCSV[i][j])))
                    tempMaxRow.append(QLabel(str(self.maxStatsCSV[i][j])))
                else:
                    minLineEdit = QLineEdit()
                    minLineEdit.setText(str(int(self.minStatsCSV[i][j])))
                    minLineEdit.setValidator(QIntValidator(0,999))
                    # minLineEdit.textEdited.connect(maxCallback)
                    maxLineEdit = QLineEdit()
                    maxLineEdit.setText(str(int(self.maxStatsCSV[i][j])))
                    maxLineEdit.setValidator(QIntValidator(0,999))
                    # maxLineEdit.textEdited.connect(self.writeMaxStatsCSV)
                    tempMinRow.append(minLineEdit)
                    tempMaxRow.append(maxLineEdit)
            self.minStatsWidgets.append(tempMinRow)
            self.maxStatsWidgets.append(tempMaxRow)
        
        minPushButton = QPushButton(_("Save min_stats.csv"))
        minCallback = partial(self.writeCSVCallback,"min")
        minPushButton.clicked.connect(minCallback)
        self.saveCSVPushbuttons.append(minPushButton)
        
        maxPushButton = QPushButton(_("Save max_stats.csv"))
        maxCallback = partial(self.writeCSVCallback,"max")
        maxPushButton.clicked.connect(maxCallback)
        self.saveCSVPushbuttons.append(maxPushButton)
        
        # Add Stats
        for i in range(len(self.addStatsCSV)):
            tempAddRow = []
            for j in range(len(self.addStatsCSV[i])):
                if ((i == 0) or (j == 0)):
                    tempAddRow.append(QLabel(str(self.addStatsCSV[i][j])))
                else:
                    addLineEdit = QLineEdit()
                    addLineEdit.setText(str(int(self.addStatsCSV[i][j])))
                    addLineEdit.setValidator(QIntValidator(0,999))
                    # addLineEdit.textEdited.connect(self.writeAddStatsCSV)
                    tempAddRow.append(addLineEdit)
            self.addStatsWidgets.append(tempAddRow)
        
        addPushButton = QPushButton(_("Save add_stats.csv"))
        addCallback = partial(self.writeCSVCallback,"add")
        addPushButton.clicked.connect(addCallback)
        self.saveCSVPushbuttons.append(addPushButton)
        
        # Order
        labels = ['a','b','c','d']
        for i in range(MAX_ENTITIES-1):
            tempOrderRow = []
            tempOrderRow.append(QLabel(labels[i]))
            tempLineEdit = QLineEdit()
            tempLineEdit.setReadOnly(True)
            tempLineEdit.setText('')
            tempOrderRow.append(tempLineEdit)
            if (i != MAX_ENTITIES-2):
                tempOrderRow.append(QLabel(u"\u2193"))
            self.orderWidgets.append(tempOrderRow)
        
        tempOrderRow = []
        tempOrderRow.append(QLabel(''))
        tempOrderRow.append(QLabel(_('Turn Order Override')))
        for i in range(MAX_TURNS-1):
            tempLineEdit = QLineEdit()
            tempLineEdit.textChanged.connect(self.turnOverrideCallback)
            tempOrderRow.append(tempLineEdit)
        self.orderWidgets.append(tempOrderRow)
        self.turnOverrideCallback()
            
        orderPushButton = QPushButton(_("Save order.csv"))
        orderCallback = partial(self.writeCSVCallback,"order")
        orderPushButton.clicked.connect(orderCallback)
        self.saveCSVPushbuttons.append(orderPushButton)
        
        # Layout
        for i in range(len(self.maxStatsWidgets)):
            for j in range(len(self.maxStatsWidgets[i])):
                self.csvScrollLayout[0].addWidget(self.minStatsWidgets[i][j],i,j)
                self.csvScrollLayout[1].addWidget(self.maxStatsWidgets[i][j],i,j)

        for i in range(len(self.addStatsWidgets)):
            for j in range(len(self.addStatsWidgets[i])):
                self.csvScrollLayout[2].addWidget(self.addStatsWidgets[i][j],i,j)
                
        for i in range(MAX_ENTITIES-1):
            self.csvScrollLayout[3].addWidget(self.orderWidgets[i][0],2*i,0)
            self.csvScrollLayout[3].addWidget(self.orderWidgets[i][1],2*i,1)
            if (i != MAX_ENTITIES-2):
                self.csvScrollLayout[3].addWidget(self.orderWidgets[i][2],(2*i)+1,1)
        for i in range(len(self.orderWidgets[4])):
            self.csvScrollLayout[3].addWidget(self.orderWidgets[4][i],i+10,1)

        # Tab
        tab2Layout = QGridLayout()
        
        tab2Layout.addWidget(self.tab2LabelObjects[0],0,0)
        tab2Layout.addWidget(self.csvScrollAreas[0],1,0)
        tab2Layout.addWidget(self.saveCSVPushbuttons[0],2,0)
        tab2Layout.addWidget(self.tab2LabelObjects[1],0,1)
        tab2Layout.addWidget(self.csvScrollAreas[1],1,1)
        tab2Layout.addWidget(self.saveCSVPushbuttons[1],2,1)
        tab2Layout.addWidget(self.tab2LabelObjects[2],3,0)
        tab2Layout.addWidget(self.csvScrollAreas[2],4,0)
        tab2Layout.addWidget(self.saveCSVPushbuttons[2],5,0)
        tab2Layout.addWidget(self.tab2LabelObjects[3],3,1)
        tab2Layout.addWidget(self.csvScrollAreas[3],4,1)
        tab2Layout.addWidget(self.saveCSVPushbuttons[3],5,1)
        self.mainTabs[1].setLayout(tab2Layout)
    
    # Tab 3 - Mults + Optimizer
    def createTab3Widget(self):
        multsBox = QWidget()
        optimizerBox = QWidget()
        
        # Scroll Areas
        self.multsScrollLayout = QGridLayout()
        
        self.multsScrollWidget = QWidget()
        self.multsScrollWidget.setLayout(self.multsScrollLayout)
        
        self.multsScrollArea = QScrollArea()
        self.multsScrollArea.setWidgetResizable(True)
        self.multsScrollArea.setWidget(self.multsScrollWidget)
        self.multsScrollLayout.setAlignment(Qt.AlignTop)
        
        self.optiScrollLayout = QGridLayout()
        
        self.optiScrollWidget = QWidget()
        self.optiScrollWidget.setLayout(self.optiScrollLayout)
        
        self.optiScrollArea = QScrollArea()
        self.optiScrollArea.setWidgetResizable(True)
        self.optiScrollArea.setWidget(self.optiScrollWidget)
        
        # Mults
        tab3Labels = ["Mults", "Optimizer"]
        tab3LabelObjects = []
        tab3LabelObjects.append(QLabel(_(tab3Labels[0])))
        
        self.multsLabels = self.multsCSV[0]
        for i in range(5):
            tempRow = []
            for j in range(21):
                if (i == 0):
                    tempRow.append(QLabel(_(self.multsLabels[j])))
                    self.unroundedMults[i][j] = self.multsLabels[j]
                else:
                    if (j == 0):
                        tempWidget = QLabel(str(i-1))
                        tempWidget.setMaximumWidth(20)
                        tempWidget.setMinimumWidth(20)
                        self.unroundedMults[i][j] = str(i-1)
                    else:
                        tempWidget = QLineEdit()
                        multsPartial = partial(self.multsChangedCallback,i,j)
                        tempWidget.textChanged.connect(multsPartial)
                        try:
                            tempWidget.setText(str(round(float(self.multsCSV[i][j]),3)))
                        except:
                            tempWidget.setText(str(self.multsCSV[i][j]))
                        self.unroundedMults[i][j] = str(self.multsCSV[i][j])
                        if (j == 1):
                            self.teamDressRows[i-1][j-1].setCurrentIndex(self.dressList.iloc[:,currentLanguage].tolist().index(str(self.multsCSV[i][j])))
                            self.dressChanged(True)
                            tempWidget.setReadOnly(True)
                            tempWidget.setMaximumWidth(150)
                            tempWidget.setMinimumWidth(150)
                        else:
                            tempWidget.setMaximumWidth(75)
                            tempWidget.setMinimumWidth(75)
                    tempRow.append(tempWidget)
            self.multsWidgets.append(tempRow)
        self.dressChangedCallback()
            
        multsPushButton = QPushButton(_("Save mults.csv"))
        multsCallback = partial(self.writeCSVCallback,"mults")
        multsPushButton.clicked.connect(multsCallback)
        self.saveCSVPushbuttons.append(multsPushButton)
            
        # Optimizer 
        tab3LabelObjects.append(QLabel(_(tab3Labels[1])))
        
        # Optimize Button
        self.optimizeButton = QPushButton(_("Run Optimizer"))
        self.optimizeButton.clicked.connect(self.runOptimizer)
        self.stopOptimizeButton = QPushButton(_("Stop Optimizer"))
        self.stopOptimizeButton.clicked.connect(self.stopOptimizer)
        
        # Standard Output
        self.optimizeOutput = QTextEdit()
        
        self.optiProcess = QProcess(self)
        self.optiProcess.setProgram(self.optimizePath)
        self.optiProcess.setWorkingDirectory(self.optimizerDir)
        self.optiProcess.readyRead.connect(self.printOptimizeOutput)
        self.optiProcess.setProcessChannelMode(QProcess.MergedChannels)
        self.optiProcess.started.connect(lambda: self.optimizeButton.setEnabled(False))
        self.optiProcess.finished.connect(lambda: self.optimizeButton.setEnabled(True))
            
        # Layout
        # Mults
        for i in range(len(self.multsWidgets)):
            for j in range(len(self.multsWidgets[i])):
                    self.multsScrollLayout.addWidget(self.multsWidgets[i][j],i,j)
        multsLayout = QGridLayout()
        multsLayout.addWidget(tab3LabelObjects[0])
        multsLayout.addWidget(self.multsScrollArea)
        multsLayout.addWidget(self.saveCSVPushbuttons[4])
        multsBox.setLayout(multsLayout)
        
        # Optimizer
        self.optiScrollLayout.addWidget(self.optimizeOutput,0,0,2,2)
        optiLayout = QGridLayout()
        optiLayout.addWidget(tab3LabelObjects[1])
        optiLayout.addWidget(self.optiScrollArea,1,0,1,2)
        optiLayout.addWidget(self.optimizeButton,2,0)
        optiLayout.addWidget(self.stopOptimizeButton,2,1)
        optimizerBox.setLayout(optiLayout)
        
        # Tab
        tab3Layout = QGridLayout()
        tab3Layout.setAlignment(Qt.AlignTop)
        tab3Layout.addWidget(multsBox,0,0,2,2)
        tab3Layout.addWidget(optimizerBox,2,0,1,2)
        self.mainTabs[2].setLayout(tab3Layout)
    
    # Tab 4 - Results
    def createTab4Widget(self):
        self.resultsScrollLayout = QGridLayout()
    
        self.resultsScrollWidget = QWidget()
        self.resultsScrollWidget.setLayout(self.resultsScrollLayout)
        
        self.resultsScrollArea = QScrollArea()
        self.resultsScrollArea.setWidgetResizable(True)
        self.resultsScrollArea.setWidget(self.resultsScrollWidget)
        self.resultsScrollLayout.setAlignment(Qt.AlignTop)
            
        for i in range(len(self.resultsCSV)):
            tempResultsRow = []
            for j in range(len(self.resultsCSV[i])):
                if ((i == 0) or (j in [2, 3, 4])):
                    tempResultsRow.append(QLabel(str(self.resultsCSV[i][j])))
                else:
                    resultsLineEdit = QLineEdit()
                    resultsLineEdit.setText(str(int(self.resultsCSV[i][j])))
                    resultsLineEdit.setReadOnly(True)
                    tempResultsRow.append(resultsLineEdit)
            self.resultsWidgets.append(tempResultsRow)
        
        self.resultsTextEdit = QTextEdit()
        try:
            self.readResultsYaml()
        except:
            pass
        
        # Layout
        for i in range(len(self.resultsWidgets)):
            for j in range(len(self.resultsWidgets[i])):
                self.resultsScrollLayout.addWidget(self.resultsWidgets[i][j],i,j)
                
        # Tab 
        tab4Layout = QGridLayout()
        
        tab4Layout.addWidget(self.resultsScrollArea,0,0)
        tab4Layout.addWidget(self.resultsTextEdit,0,1)
        self.mainTabs[3].setLayout(tab4Layout)
    
    # Tab 5 - Statistics
    # Tab 6 - Extras
    def createTab6Widget(self):
        # Scroll Areas 
        extraScrollAreas = []
        extraScrollLayout = []
        for i in range(3):
            scrollLayout = QGridLayout()
        
            scrollWidget = QWidget()
            scrollWidget.setLayout(scrollLayout)
            
            scrollArea = QScrollArea()
            scrollArea.setWidgetResizable(True)
            scrollArea.setWidget(scrollWidget)
            scrollLayout.setAlignment(Qt.AlignTop)
            
            extraScrollAreas.append(scrollArea)
            extraScrollLayout.append(scrollLayout)
            
        # Speed Linking
        self.speedLinkingWidgets = []
        tempLabel = QLabel(_('Speed Linking:'))
        tempLabel.setMaximumWidth(100)
        self.speedLinkingWidgets.append(tempLabel)
        self.speedLinkingWidgets.append(QLabel(_('Gauger Speed:')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,999))
        tempLineEdit.textChanged.connect(self.speedLinkCallback)
        self.speedLinkingWidgets.append(tempLineEdit)
        tempLabel = QLabel(_('Gauge Boost (%):'))
        tempLabel.setMaximumWidth(100)
        self.speedLinkingWidgets.append(tempLabel)
        tempLineEdit = QLineEdit()
        tempLineEdit.setText('20')
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,999))
        tempLineEdit.textChanged.connect(self.speedLinkCallback)
        self.speedLinkingWidgets.append(tempLineEdit)
        self.speedLinkingWidgets.append(QLabel(_('AOE Speed Buff?')))
        tempCheckBox = QCheckBox()
        tempCheckBox.clicked.connect(self.speedLinkCallback)
        self.speedLinkingWidgets.append(tempCheckBox)
        self.speedLinkingWidgets.append(QLabel(_('Num. Turns between:')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setText('3')
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,9))
        tempLineEdit.textChanged.connect(self.speedLinkCallback)
        self.speedLinkingWidgets.append(tempLineEdit)
        self.speedLinkingWidgets.append(QLabel(_('Min. Speed Required:')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,999))
        tempLineEdit.setReadOnly(True)
        self.speedLinkingWidgets.append(tempLineEdit)
        
        speedLinkArray = [[0,0], [1,0], [1,1], [2,0], [2,1], [3,0], [3,1], [4,0], [4,1], [5,0], [5,1]]
        
        # Debuff Numbers
        self.debuffNumWidgets = []
        tempLabel = QLabel(_('Debuff Numbers:'))
        tempLabel.setMaximumWidth(100)
        self.debuffNumWidgets.append(tempLabel)
        self.debuffNumWidgets.append(QLabel(_('Chance Per Hit (%):')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,99))
        tempLineEdit.textChanged.connect(self.debuffNumCallback)
        self.debuffNumWidgets.append(tempLineEdit)
        tempLabel = QLabel(_('Number Hits:'))
        tempLabel.setMaximumWidth(100)
        self.debuffNumWidgets.append(tempLabel)
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QIntValidator(0,9))
        tempLineEdit.textChanged.connect(self.debuffNumCallback)
        self.debuffNumWidgets.append(tempLineEdit)
        self.debuffNumWidgets.append(QLabel(_('At Least One Success?')))
        tempCheckBox = QCheckBox()
        tempCheckBox.clicked.connect(self.debuffNumCallback)
        self.debuffNumWidgets.append(tempCheckBox)
        self.debuffNumWidgets.append(QLabel(_('% Hits with Debuff:')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(80)
        tempLineEdit.setValidator(QDoubleValidator(0,999,3))
        tempLineEdit.setReadOnly(True)
        self.debuffNumWidgets.append(tempLineEdit)
        
        debuffNumArray = [[0,0], [1,0], [1,1], [2,0], [2,1], [3,0], [3,1], [4,0], [4,1]]
        
        # Turn Constraint Calculator
        self.turnConstraintWidgets = []
        
        turns = []
        labels = ['a','b','c','d']
        unitLabel = ['Unit', 'before unit']
        for i in range(0,11):
            turns.append(str(i))
        tempLabel = QLabel(_('Turn Constraint Calculator'))
        tempLabel.setMaximumWidth(200)
        self.turnConstraintWidgets.append(tempLabel)
        for i in range(2):
            tempLabel = QLabel(_(unitLabel[i]))
            tempLabel.setMaximumWidth(60)
            tempLabel.setMinimumWidth(60)
            self.turnConstraintWidgets.append(tempLabel)
            tempComboBox = QComboBox()
            tempComboBox.addItems(labels)
            tempComboBox.setMaximumWidth(40)
            tempComboBox.setMinimumWidth(40)
            tempComboBox.currentIndexChanged.connect(self.turnConstraintCallback)
            self.turnConstraintWidgets.append(tempComboBox)
            tempLabel = QLabel(_('gets'))
            tempLabel.setMaximumWidth(25)
            tempLabel.setMinimumWidth(25)
            self.turnConstraintWidgets.append(tempLabel)
            tempComboBox = QComboBox()
            tempComboBox.addItems(turns)
            tempComboBox.setMaximumWidth(40)
            tempComboBox.setMinimumWidth(40)
            tempComboBox.currentIndexChanged.connect(self.turnConstraintCallback)
            self.turnConstraintWidgets.append(tempComboBox)
            tempLabel = QLabel(_('turn(s) with gauge-up of'))
            tempLabel.setMaximumWidth(120)
            tempLabel.setMinimumWidth(120)
            self.turnConstraintWidgets.append(tempLabel)
            tempLineEdit = QLineEdit()
            tempLineEdit.textChanged.connect(self.turnConstraintCallback)
            tempLineEdit.setValidator(QIntValidator(0,999))
            tempLineEdit.setMaximumWidth(40)
            self.turnConstraintWidgets.append(tempLineEdit)
            self.turnConstraintWidgets.append(QLabel('%'))
        self.turnConstraintWidgets.append(QLabel(_('Result:')))
        tempLineEdit = QLineEdit()
        tempLineEdit.setMaximumWidth(120)
        tempLineEdit.setReadOnly(True)
        self.turnConstraintWidgets.append(tempLineEdit)
        
        turnConstraintArray = [[0,0], [1,0], [1,1], [1,2], [1,3], [1,4], [1,5], [1,6], [2,0], [2,1], [2,2], [2,3], [2,4], [2,5], [2,6], [3,0], [3,1]]
        
        # Layout
        for i in range(1,len(self.speedLinkingWidgets)):
            extraScrollLayout[0].addWidget(self.speedLinkingWidgets[i],speedLinkArray[i][0],speedLinkArray[i][1])
            
        for i in range(1,len(self.debuffNumWidgets)):
            extraScrollLayout[1].addWidget(self.debuffNumWidgets[i],debuffNumArray[i][0],debuffNumArray[i][1])
            
        for i in range(1,len(self.turnConstraintWidgets)):
            extraScrollLayout[2].addWidget(self.turnConstraintWidgets[i],turnConstraintArray[i][0],turnConstraintArray[i][1])
            if (i == len(self.turnConstraintWidgets) - 1):
                extraScrollLayout[2].addWidget(self.turnConstraintWidgets[i],turnConstraintArray[i][0],turnConstraintArray[i][1],1,3)
        
        # Tab
        tab6Layout = QGridLayout()
        tab6Layout.addWidget(self.speedLinkingWidgets[0],0,0)
        tab6Layout.addWidget(extraScrollAreas[0],1,0)
        tab6Layout.addWidget(self.debuffNumWidgets[0],0,1)
        tab6Layout.addWidget(extraScrollAreas[1],1,1)
        tab6Layout.addWidget(self.turnConstraintWidgets[0],2,0)
        tab6Layout.addWidget(extraScrollAreas[2],3,0)
        
        self.mainTabs[5].setLayout(tab6Layout)

    # Tab 7 - Data
    def createTab7Widget(self):
        self.dataTabWidget = QTabWidget()
        self.dataTabWidget.setSizePolicy(QSizePolicy.Preferred,QSizePolicy.Ignored)
        self.dataTabLabels = []
        
        # Dresses
        self.dataDressTab = QWidget()

        self.dataDressWidget = QWidget()
        self.dataDressLayout = QGridLayout()
        self.dressSearchLayout = QGridLayout()
        self.dataTabLabels.append('dress.csv')
        self.dressSearchLayout.addWidget(QLabel(_('Search:')),0,0)
        self.dressSearchLayout.addWidget(QLineEdit(),0,1)
        self.dataDressWidget.setLayout(self.dressSearchLayout)
        self.dataDressLayout.addWidget(self.dataDressWidget)
        
        self.dressTableWidget = QWidget()
        self.createDressTab()
        
        
        self.dataDressTab.setLayout(self.dataDressLayout)
        
        # Orbs
        self.dataOrbTab = QWidget()
        self.dataTabLabels.append('orb.csv')
        self.dressSearchLayout.addWidget(QLabel(_('Search:')),0,0)
        self.dressSearchLayout.addWidget(QLineEdit(),0,1)
        #self.createOrbTab()
        
        # db
        self.dataDBTab = QWidget()
        self.dataTabLabels.append('db.csv')
        self.dressSearchLayout.addWidget(QLabel(_('Search:')),0,0)
        self.dressSearchLayout.addWidget(QLineEdit(),0,1)
        #self.createDBTab()
        
        # Skill Mods
        self.dataSkillModsTab = QWidget()
        self.dataTabLabels.append('Skill Mods')
        self.dressSearchLayout.addWidget(QLabel(_('Search:')),0,0)
        self.dressSearchLayout.addWidget(QLineEdit(),0,1)
        #self.createSkillModsTab()
        
        self.dataTabWidget.addTab(self.dataDressTab, _(self.dataTabLabels[0]))
        self.dataTabWidget.addTab(self.dataOrbTab, _(self.dataTabLabels[1]))
        self.dataTabWidget.addTab(self.dataDBTab, _(self.dataTabLabels[2]))
        self.dataTabWidget.addTab(self.dataSkillModsTab, _(self.dataTabLabels[3]))
        
        tab7Layout = QHBoxLayout()
        tab7Layout.addWidget(self.dataTabWidget)
        self.mainTabs[6].setLayout(tab7Layout)
    
    # Tab 8 - Settings
    def createTab8Widget(self):
        # Language
        languageLabel = QLabel(_("Language:"))
        self.languageComboBox = QComboBox()
        self.languageComboBox.addItems(self.languageListTranslated)
        self.languageComboBox.activated[str].connect(self.changeLanguage)
        
        # Layout
        tab7Layout = QGridLayout()
        tab7Layout.addWidget(languageLabel,0,0)
        tab7Layout.addWidget(self.languageComboBox,0,1)
        
        self.mainTabs[7].setLayout(tab7Layout)
    
    # Language Functions
    def changeLanguage(self, text):
        for i in self.languageList:
            if text == _(i):
                setLanguage(i)
        self.refreshLabels()
        
    # Turn Simulator Functions
    def createTurnSimLabels(self):
        numberCol = 31
        # Strings for Labels
        self.labelStringArray.append(["Speed - Up/Down", "Gauge - Up/Down", "Turn", "                    Dress                    ", "Skill", "ATK-Up", "DEF-Up", "SPD-Up", \
                                      "CRT-Up", "DEF-Down", "Burned", "Frozen", "SF Bonus", "Forced Crit", "Forced Heavy", "Ignore DEF", \
                                      "Elem. Adv.", "Other Skill Bonus", "ATK Mult", "HP Mult", "DEF Mult", "SPD Mult"])
        # Appending Widgets to Row
        labelRow = []
        counter = 0
        for i in range(numberCol):
            if (i in [0, 1, 2, 4, 5, 6, 7, 9, 10]):
                labelRow.append(None)
            elif (i in [3, 8, 11, 12, 13, 26, 27, 28, 29, 30]):
                tempLabel = QLabel(_(self.labelStringArray[2][counter]))
                if (i in [3, 8]):
                    tempLabel.setAlignment(Qt.AlignCenter)
                labelRow.append(tempLabel)
                counter = counter + 1
            else:
                pushButton = QPushButton(_(self.labelStringArray[2][counter]))
                pushButtonCall = partial(self.pushButtonSelectAll, self.labelStringArray[2][counter])
                pushButton.clicked.connect(pushButtonCall)
                labelRow.append(pushButton)
                counter = counter + 1

        # Append Row to Row List
        self.turnSimRows.append(labelRow)
            
    def enoughInputs(self):
        # Checks if the minimum amount of settings are selected for simulating turns
        global MAX_ENTITIES
        # Turns
        if self.numberTurnsLineEdit.text() == "":
            return False
        # Target
        if self.targetSpeedLineEdit.text() == "":
            return False
        #if self.targetsLineEdit.text() == "":
        #    return False
        #if self.targetDefLineEdit.text() == "":
        #    return False
        # Dresses
        counter = 0
        self.currentDresses = [True, True, True, True, True]
        for i in range(MAX_ENTITIES-1):
            if ((str(self.teamDressRows[i][0].currentText()) != "") & (str(self.teamDressRows[i][6].text()) != "")):
                counter = counter + 1
            else:
                self.currentDresses[i] = False
        if (counter < 1):
            return False
        counter = 0
        return True
           
    def calculateMults(self, turn):
        # Check if there enough inputs
        if (self.enoughInputs() == False):
            return
            
        # Initialize Lists
        multsInput = []
        multsList = []
        statBuffList = []
        skillModList = []
        
        # Collect inputs for mults calculations
        try:
            multsInput.append(int(self.targetsLineEdit.text()))
        except:
            multsInput.append(0.0)
        try:
            multsInput.append(float(self.targetDefLineEdit.text()))
        except:
            multsInput.append(0.0)
        try:
            multsInput.append(float(self.noCritMultLineEdit.text()))
        except:
            multsInput.append(0.0)

        # Skill Related Mods
        skillHitKeys = ["s1 hits", "s2 hits", "s3 hits"]
        skillAOEKeys = ["s1 aoe", "s2 aoe", "s3 aoe"]
        skillModKeys = ["s1 mod", "s2 mod", "s3 mod"]
        skillSpeKeys = ["s1 special", "s2 special", "s3 special"]
        skillSModKeys = ["s1 smod", "s2 smod", "s3 smod"]
        skillSModValues = ["self hp", "self def", "self spd"]
        
        
        dressIndex = self.teamDressRows[self.turnOrder[turn]][0].currentIndex()
        dressDBIndex = self.skillMods["Dress"].values.tolist().index(str(self.dressTranslator["DB name"][dressIndex]))
        whichSkill = int(self.turnSimRows[turn+1][13].text())
        
        skillHits = float(self.skillMods[skillHitKeys[whichSkill-1]][dressDBIndex])
        multsInput.append(skillHits)
        
        try:
            skillAOE = str(self.skillMods[skillAOEKeys[whichSkill-1]][dressDBIndex])
            if not is_float(skillAOE):
                if (skillAOE == 'TRUE'):
                    skillAOE = 1.0
                else:
                    skillAOE = 0.0
            else:
                skillAOE = float(skillAOE)
        except:
            skillAOE = 0.0
        multsInput.append(skillAOE)
        
        skillMod = float(self.skillMods[skillModKeys[whichSkill-1]][dressDBIndex])
        skillModList.append(skillMod)
        for i in range(len(skillSModValues)):
            if skillSModValues[i] in str(self.skillMods[skillSpeKeys[whichSkill-1]][dressDBIndex]).lower():
                skillModList.append(float(self.skillMods[skillSModKeys[whichSkill-1]][dressDBIndex]))
            else:
                skillModList.append(0.0)
        multsInput.append(skillModList)
        
        try:
            skillLvBonus = float(self.teamDressRows[self.turnOrder[turn]][1+whichSkill].text())
        except:
            skillLvBonus = 0.0
        multsInput.append(skillLvBonus)
        
        # Buffs
        try:
            statBuffList.append(float(self.turnSimRows[turn+1][14].text()))       # ATK
        except:
            statBuffList.append(0.0)
        statBuffList.append(0.0)                                                  # HP
        try:
            statBuffList.append(float(self.turnSimRows[turn+1][15].text()))       # DEF
        except:
            statBuffList.append(0.0)
        try:
            statBuffList.append(float(self.turnSimRows[turn+1][16].text()))       # SPD
        except:
            statBuffList.append(0.0)
        multsInput.append(statBuffList)
        try:
            multsInput.append(float(self.turnSimRows[turn+1][17].text()))
        except:
            multsInput.append(0.0)
        
        # Debuffs
        for i in range(3):
            # DEF, Burn, Freeze
            try:
                multsInput.append(float(self.turnSimRows[turn+1][18+i].text()))
            except:
                multsInput.append(0.0)
            
        # Other Bonuses
        for i in range(6):
            # SF Bonus, Forced Crit, Forced Heavy, Ignore DEF, Elem. Adv., Other Skill Bonuses
            try:
                multsInput.append(float(self.turnSimRows[turn+1][21+i].text()))
            except:
                multsInput.append(0.0)
                
        # Event Bonuses
        try:
            multsInput.append(float(self.teamDressRows[self.turnOrder[turn]][len(self.teamDressRows[self.turnOrder[turn]])-1].text())/100)
        except:
            multsInput.append(0.0)
        
        
        # Mults Calculations
        mults = Mults(multsInput)
        multsList.append(mults.mult)
        multsList.append(mults.ccMult)
        multsList.append(mults.cdMult)
        multsList.append(mults.cccdMult)
        if (len(self.multsArray) == len(self.turnOrder)):
            self.multsArray[turn] = multsList
        else:
            self.multsArray.append(multsList)
        
        # Set the rows in turn sim
        for i in range(len(self.multsArray[turn][0])):
            if (is_float(self.multsArray[turn][0][i])):
                self.turnSimRows[turn+1][27+i].setText(str(round(self.multsArray[turn][0][i],3)))
            else:
                self.turnSimRows[turn+1][27+i].setText(str(0.0))
        
    def recalculateMults(self, text):
        # Check if there enough inputs
        if (self.enoughInputs() == False):
            return
        # Check if it is recalculating all of them or just one
        if (text.isnumeric()):
            if (self.turnOrder[int(text)] != (MAX_ENTITIES-1)):
                self.calculateMults(int(text))
        else:
            for i in range(len(self.turnOrder)):
                if (self.turnOrder[i] != (MAX_ENTITIES-1)):
                    self.calculateMults(i)
        self.updateMults()
                    
    def pushButtonSelectAll(self, text):
        buttonIndex = self.labelStringArray[2].index(text)+9
        numberSelected = 0
        for i in range(MAX_TURNS-1):
            if self.turnSimRows[i+1][buttonIndex].text() != "":
                numberSelected = numberSelected + 1
        if ((numberSelected/(MAX_TURNS-1)) > 0.5):
            newValue = ""
        else:
            newValue = "1.0"
        for i in range(MAX_TURNS-1):
            self.turnSimRows[i+1][buttonIndex].setText(newValue)
        self.recalculateMults("all")
        
    def dressChanged(self,editSkillMods,whichSkillMod=-1):
        global MAX_TURNS
        global MAX_ENTITIES
        skillOtherKeys = ["s1 other", "s2 other", "s3 other"]
        skillBonusKeys = ["s1 bonus", "s2 bonus", "s3 bonus"]
        
        # Modify Skill Bonuses to Max Value
        if (editSkillMods):
            if whichSkillMod not in [0,1,2]:
                whichSkillMod = [0,1,2,3]
            else:
                temp = whichSkillMod
                whichSkillMod = []
                whichSkillMod.append(temp)
            for dress in whichSkillMod:
                dressIndex = self.teamDressRows[dress][0].currentIndex()
                if (dressIndex == -1):
                    continue
                if (dressIndex != 0):
                    dressDBIndex = self.skillMods["Dress"].values.tolist().index(str(self.dressTranslator["DB name"][dressIndex]))
                    for sk in range(3):
                        skillBonus = str(self.skillMods[skillBonusKeys[sk]][dressDBIndex])
                        if (is_float(skillBonus) == True):
                            skillBonus = str(int(float(skillBonus)*100))
                            self.teamDressRows[dress][2+sk].setText(skillBonus)
                        else:
                            self.teamDressRows[dress][2+sk].setText('')
        # Modify Turn Simulator
        for turn in range(len(self.turnOrder)):
            if self.turnOrder[turn] != MAX_ENTITIES-1:
                currDress = self.teamDressRows[self.turnOrder[turn]][0].currentText()
                rowDress = self.turnSimRows[turn+1][12].text()
                if currDress != rowDress:
                    self.turnSimRows[turn+1][12].setText(currDress)
                    # Set the other skill bonus
                    dressIndex = self.teamDressRows[self.turnOrder[turn]][0].currentIndex()
                    dressDBIndex = self.skillMods["Dress"].values.tolist().index(str(self.dressTranslator["DB name"][dressIndex]))
                    whichSkill = int(self.turnSimRows[turn+1][13].text())
                    otherBonus = str(self.skillMods[skillOtherKeys[whichSkill-1]][dressDBIndex])
                    if (is_float(otherBonus) == False):
                        otherBonus = "0.0"
                    self.turnSimRows[turn+1][26].setText(otherBonus)
            else:
                self.turnSimRows[turn+1][12].setText("")
            self.turnSimRows[turn+1][12].setPalette(self.widgetColors[self.turnOrder[turn]])
            for i in range(14,len(self.turnSimRows[turn+1])-4):
                self.turnSimRows[turn+1][i].setPalette(self.widgetColors[self.turnOrder[turn]])
        for notTurn in range(len(self.turnOrder),MAX_TURNS-1):
            self.turnSimRows[notTurn+1][12].setText("")
            self.turnSimRows[notTurn+1][12].setPalette(self.widgetColors[MAX_ENTITIES-1])
            for i in range(14,len(self.turnSimRows[notTurn])-4):
                self.turnSimRows[notTurn+1][i].setPalette(self.widgetColors[MAX_ENTITIES-1])
        self.calculateOrder()
    
    def dressChangedCallback(self,whichDress=-1):
        if (self.enoughInputs() == False):
            self.dressChanged(True,whichDress)
        else:
            self.simulateTurns()
            self.dressChanged(True,whichDress)

    def sk4Changed(self):
        try:
            match self.s4BonusComboBox.currentText():
                case 'ATK':
                    self.multsWidgets[0][2].setText('S4-ATK')
                case 'HP':
                    self.multsWidgets[0][2].setText('S4-HP')
                case 'DEF':
                    self.multsWidgets[0][2].setText('S4-DEF')
        except:
            pass

    def createBlankTurnSimRow(self):
        global MAX_ENTITIES
        blankRow = []
        for i in range(len(self.turnSimRows[1])):
            if i in range(1,MAX_ENTITIES+1):
                blankRow.append(0)
            else:
                blankRow.append("")
        return blankRow
        
    def setBlankSimRowText(self, row, keepStatusEffects):
        rowText = self.createBlankTurnSimRow()
        global MAX_ENTITIES
        for j in range(len(self.turnSimRows[row])):
            if j == 0:
                continue
            elif j in range(1,MAX_ENTITIES+1):
                if (not keepStatusEffects):
                    self.turnSimRows[row][j].setCurrentIndex(rowText[j])
            else:
                if ((not keepStatusEffects) or (j in [11,12,13, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30])):
                    self.turnSimRows[row][j].setText(rowText[j])
    
    def setTurnSimRowVisibility(self, row, visible):
        global MAX_ENTITIES
        for j in range(len(self.turnSimRows[row])):
            self.turnSimRows[row][j].setVisible(visible)
    
    def resetTurnSimSelections(self):
        global MAX_TURNS
        for i in range(MAX_TURNS-1):
            self.setBlankSimRowText(i+1, False)
        self.simulateTurns()
    
    def loadTurnSimSelections(self):
        filepath = os.path.join('turn_sim_selections.csv')
        try:
            teamTurnSelections = pd.read_csv(filepath)
            teamTurnSelections = teamTurnSelections.T.reset_index().values.T.tolist()
        except:
            self.errorMsgPopup(_("Failed to load, \"turn_sim_selections.csv\""))
            return
        
        # Change nan to ''
        for i in range(len(teamTurnSelections)):
            for j in range(len(teamTurnSelections[i])):
                try:
                    if math.isnan(float(teamTurnSelections[i][j])):
                        teamTurnSelections[i][j] = ''
                except:
                    pass
        # Set Selections
        for i in range(len(self.teamLineEdits)):
            self.teamLineEdits[i].setText(teamTurnSelections[0][i])
        
        for i in range(MAX_ENTITIES-1):
            self.teamDressRows[i][0].setCurrentIndex(int(teamTurnSelections[i+1][0]))
        for i in range(len(self.teamDressRows)):
            for j in range(1,len(self.teamDressRows[i])):
                if ((j > 6) and (j != len(self.teamDressRows[i])-1)):
                        self.teamDressRows[i][j].setCurrentIndex(int(teamTurnSelections[i+1][j]))
                else:
                    try:
                        self.teamDressRows[i][j].setText(str(int(teamTurnSelections[i+1][j])))
                    except:
                        pass

        for i in range(1,len(self.turnSimRows)):
            for j in range(1,len(self.turnSimRows[i])):
                if (j > 0) and (j < 6):
                    self.turnSimRows[i][j].setCurrentIndex(int(teamTurnSelections[i+4][j-1]))
                elif (j == 11) or (j == 13):
                    try:
                        self.turnSimRows[i][j].setText(str(int(teamTurnSelections[i+4][j-1])))
                    except:
                        self.turnSimRows[i][j].setText('')
                else:
                    self.turnSimRows[i][j].setText(str(teamTurnSelections[i+4][j-1]))
        self.simulateTurns()
        
    def saveTurnSimSelections(self):
        teamTurnSelections = []
        
        tempRow = []
        for i in range(len(self.teamLineEdits)):
            tempRow.append(self.teamLineEdits[i].text())
        teamTurnSelections.append(tempRow)
        
        for i in range(len(self.teamDressRows)):
            tempRow = []
            for j in range(len(self.teamDressRows[i])):
                if (j == 0) or ((j > 6) and (j != len(self.teamDressRows[i])-1)):
                    tempRow.append(self.teamDressRows[i][j].currentIndex())
                else:
                    try:
                        tempRow.append(self.teamDressRows[i][j].text())
                    except:
                        tempRow.append(None)
            teamTurnSelections.append(tempRow)
        
        for i in range(1,len(self.turnSimRows)):
            tempRow = []
            for j in range(1,len(self.turnSimRows[i])):
                if (j > 0) and (j < 6):
                    tempRow.append(self.turnSimRows[i][j].currentIndex())
                else:
                    tempRow.append(self.turnSimRows[i][j].text())
            teamTurnSelections.append(tempRow)
        
        filepath = os.path.join('turn_sim_selections.csv')
        try:
            self.writeCSV(filepath,teamTurnSelections)
        except:
            self.errorMsgPopup(_("Failed to save CSV file. Please make sure no other program has the CSV file open."))
    
    def simulateTurns(self):
        # Simulates the turns and restructures turn simulation array
        # Check if there are enough inputs to simulate turns
        if (self.enoughInputs() == False):
            self.dressChanged()
            return
        
        # Simulate turn order with current selections
        self.turnOrder = []
        self.createSpeedTurnArray()
        self.multsArray  = []
        
        # Compare current array with new turn order and restructure as needed
        # Which turn each entity is on
        turnCountArray = [0,0,0,0,0]
        playerTurn = 1
        for i in range(MAX_TURNS-1):
            for j in range(MAX_ENTITIES-1):
                if (i == 0):
                    self.teamDressRows[j][i+7].setEnabled(False)
                    self.teamDressRows[j][i+7].setVisible(True)
                    self.skillOrderLabelWidgets[i].setVisible(True)
                else:
                    self.teamDressRows[j][i+7].setVisible(False)
                    self.skillOrderLabelWidgets[i].setVisible(False)
            if i in range(len(self.turnOrder)):
                self.setTurnSimRowVisibility(i+1, True)
                whichDress = self.turnOrder[i]
                currDressTurn = turnCountArray[whichDress]
                if whichDress != (MAX_ENTITIES-1):
                    self.turnSimRows[i+1][11].setText(str(playerTurn))
                    self.turnSimRows[i+1][12].setText(self.teamDressRows[whichDress][0].currentText())
                    try:
                        self.turnSimRows[i+1][13].setText(self.teamDressRows[whichDress][currDressTurn + 7].currentText())
                    except:
                        self.turnSimRows[i+1][13].setText("1")
                    if (currDressTurn == 0):
                        self.teamDressRows[whichDress][currDressTurn+7].setEnabled(True)
                    else:
                        self.teamDressRows[whichDress][currDressTurn + 7].setVisible(True)
                        self.skillOrderLabelWidgets[currDressTurn].setVisible(True)
                    self.calculateMults(i)
                    playerTurn = playerTurn + 1
                    turnCountArray[whichDress] = currDressTurn + 1
                else:
                    self.setBlankSimRowText(i+1, True)
                    self.multsArray.append(0)
                    turnCountArray[whichDress] = currDressTurn + 1
            else:
                self.setBlankSimRowText(i+1, True)
                self.setTurnSimRowVisibility(i+1, False)
        self.dressChanged(False)
        self.updateMults()
        self.sk4Changed()
        #self.errorMsgPopup(_("Failed to calculate Mults. Please make sure the .xlsx sheet is up-to-date and dress names are correct."))
                    
    def createSpeedTurnArray(self):
        global MAX_ENTITIES
        # Determines the turn order
        totalTurns = int(self.numberTurnsLineEdit.text())
        turnsRemaining = totalTurns
        # This turn counter counts the targets move as well
        turn = 0
        prevGauge = np.array([0, 0, 0, 0, 0])
        while (turnsRemaining):
            tick = 1
            newGauge = np.array([0, 0, 0, 0, 0])
            # Gauge Changes
            gaugeDelta = []
            for i in range(MAX_ENTITIES):
                try:
                    gaugeDelta.append(int(self.turnSimRows[turn+1][6+i].text()))
                except:
                    gaugeDelta.append(0)
            gaugeDelta = 100*np.array(gaugeDelta)
            prevGauge = np.add(prevGauge,gaugeDelta)
            # Check if any Gauge is below 0 and set to 0 if true
            prevGauge[prevGauge < 0.0] = 0.0
            # Get Speed Changes
            speedMults = []
            for i in range(MAX_ENTITIES):
                try: 
                    if (self.turnSimRows[turn+1][1+i].currentText() == u"\u2191"):
                        speedMults.append(1.3)
                    elif (self.turnSimRows[turn+1][1+i].currentText() == u"\u2193"):
                        speedMults.append(0.7)
                    else:
                        speedMults.append(1)
                except:
                    speedMults.append(1)
            # Gauge gain per tick
            gaugeGain = []
            for i in range(MAX_ENTITIES):
                try:
                    if i != MAX_ENTITIES-1:
                        if (self.currentDresses[i]):
                            gaugeGain.append(int(self.teamDressRows[i][6].text()))
                        else:
                            gaugeGain.append(0)
                    else:
                        gaugeGain.append(int(self.targetSpeedLineEdit.text()))
                except:
                    gaugeGain.append(0)
            gaugeGain = np.array(gaugeGain)
            gaugeGain = gaugeGain * speedMults
            newGauge = np.add(prevGauge, gaugeGain)
            while (all(x<10000 for x in newGauge)):
                newGauge = np.add(newGauge, gaugeGain)
                tick = tick + 1
            tempList = np.ndarray.tolist(newGauge)
            whichDress = tempList.index(max(tempList))
            self.turnOrder.append(whichDress)
            prevGauge = newGauge
            prevGauge[whichDress] = 0
            turn = turn + 1
            if whichDress != MAX_ENTITIES-1:
                turnsRemaining = turnsRemaining - 1
            if (turn == (MAX_TURNS-1)):
                break
            
    def setCheckBox(self, row, column):
        if is_float(self.turnSimRows[row+1][column+14].text()):
            if (float(self.turnSimRows[row+1][column+14].text()) == 0.0):
                self.turnSimRows[row+1][column+14].setText('1.0')
            else:
                self.turnSimRows[row+1][column+14].setText('')
        else:
                self.turnSimRows[row+1][column+14].setText('1.0')
        self.recalculateMults(str(row))
    
    # Mults + Optimizer Functions
    def writeCSVCallback(self, which):
        try:
            match which:
                case "mults":
                    self.writeMultsCSV()
                case "min":
                    self.writeMinStatsCSV()
                case "max":
                    self.writeMaxStatsCSV()
                case "add":
                    self.writeAddStatsCSV()
                case "order":
                    self.writeOrderTXT()
                case "all":
                    self.writeMultsCSV()
                    self.writeMinStatsCSV()
                    self.writeMaxStatsCSV()
                    self.writeAddStatsCSV()
                    self.writeOrderTXT()
            return True
        except:
            self.errorMsgPopup(_("Failed to save CSV file. Please make sure no other program has the CSV file open."))
            return False
    
    def writeCSV(self,filepath,data):
        dataFrame = pd.DataFrame(data)
        dataFrame.to_csv(filepath,header=False,index=False)
    
    def updateMults(self):
        global MAX_ENTITIES
        
        # If starting up do not update the mults widgets
        if (STARTUP):
            return
        
        # Unrounded Mults Array
        self.unroundedMults = np.zeros((len(self.multsCSV), len(self.multsCSV[0])))
        self.unroundedMults = self.unroundedMults.tolist()
        for i in range(len(self.multsLabels)):
            self.unroundedMults[0][i] = self.multsLabels[i]
        for i in range(MAX_ENTITIES-1):
            self.unroundedMults[i+1][0] = str(i)
        
        # Create Per Dress Mults
        dressMults = np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]], dtype='f')
        dressCCMults = np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]], dtype='f')
        dressCDMults = np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]], dtype='f')
        dressCCCDMults = np.array([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]], dtype='f')
        for i in range(len(self.turnOrder)):
            whichDress = self.turnOrder[i]
            if (whichDress != MAX_ENTITIES-1):
                dressMults[whichDress] = np.add(dressMults[whichDress], self.multsArray[i][0])
                dressCCMults[whichDress] = np.add(dressCCMults[whichDress], self.multsArray[i][1])
                dressCCMults[whichDress] = np.subtract(dressCCMults[whichDress], self.multsArray[i][0])
                dressCDMults[whichDress] = np.add(dressCDMults[whichDress], self.multsArray[i][2])
                dressCDMults[whichDress] = np.subtract(dressCDMults[whichDress], self.multsArray[i][0])
                dressCCCDMults[whichDress] = np.add(dressCCCDMults[whichDress], self.multsArray[i][3])
                dressCCCDMults[whichDress] = np.subtract(dressCCCDMults[whichDress], self.multsArray[i][0])
        
        # Fill in mults widgets
        for i in range(4):
            # Dress
            self.multsWidgets[i+1][1].setText(self.teamDressRows[i][0].currentText())
            self.unroundedMults[i+1][1] = self.teamDressRows[i][0].currentText()
            if (self.currentDresses[i]):
                self.multsWidgets[i+1][1].setPalette(self.widgetColors[i])
            else:
                self.multsWidgets[i+1][1].setPalette(self.widgetColors[4])
            # S4 Bonus
            try:
                self.multsWidgets[i+1][2].setText(str(float(self.teamDressRows[i][5].text())/100))
                self.unroundedMults[i+1][2] = str(float(self.teamDressRows[i][5].text())/100)
            except:
                self.multsWidgets[i+1][2].setText(self.teamDressRows[i][5].text())
                self.unroundedMults[i+1][2] = self.teamDressRows[i][5].text()
            # Stat Mults
            for stat in range(4):
                # Mults
                self.multsWidgets[i+1][3+stat].setText(str(round(dressMults[i][stat],3)))
                self.unroundedMults[i+1][3+stat] = dressMults[i][stat]
                # CC Mults
                self.multsWidgets[i+1][9+stat].setText(str(round(dressCCMults[i][stat],3)))
                self.unroundedMults[i+1][9+stat] = dressCCMults[i][stat]
                # CD Mults
                self.multsWidgets[i+1][13+stat].setText(str(round(dressCDMults[i][stat],3)))
                self.unroundedMults[i+1][13+stat] = dressCDMults[i][stat]
                # CCCD Mults
                self.multsWidgets[i+1][17+stat].setText(str(round(dressCCCDMults[i][stat],3)))
                self.unroundedMults[i+1][17+stat] = dressCCCDMults[i][stat]
        # Fill in Gaps
        for i in range(4):
            for j in range(len(self.multsWidgets[i])):
                if (is_float(self.multsWidgets[i+1][j].text()) == False):
                    if (j != 1):
                        self.multsWidgets[i+1][j].setText("0.0")
                    else:
                        if (self.currentDresses[i] == False):
                            for dress in range(4):
                                if (self.currentDresses[dress] == True):
                                    self.multsWidgets[i+1][j].setText(self.teamDressRows[dress][0].currentText())

    def multsChangedCallback(self,row,col):
        try:
            self.unroundedMults[row][col] = self.multsWidgets[row][col].text()
        except:
            pass
    
    def writeMultsCSV(self):
        filepath = os.path.join(self.optimizerDir + '/' + 'mults.csv')
        self.writeCSV(filepath,self.unroundedMults)
    
    def runOptimizer(self):
        if (self.writeCSVCallback("all")):
            self.optimizeButton.setText(_("Optimizer is Running Please Wait"))
            self.optiProcess.start()
        else:
            print('test')
        
    def stopOptimizer(self):
        try:
            try:
                self.optiProcess.stop()
                self.optimizeButton.setText(_("Run Optimizer"))
            except:
                self.optiProcess.kill()
                self.optimizeButton.setText(_("Run Optimizer"))
        except:
            pass
        
    def printOptimizeOutput(self):
        try:
            optiTextCursor = self.optimizeOutput.textCursor()
            optiTextCursor.movePosition(optiTextCursor.End)
            optiTextCursor.insertText(self.optiProcess.readAll().data().decode())
            self.optimizeOutput.ensureCursorVisible()
        except:
            optiTextCursor = self.optimizeOutput.textCursor()
            optiTextCursor.movePosition(optiTextCursor.End)
            optiTextCursor.insertText("Optimizer Finished: Failed to Print Output")
            self.optimizeOutput.ensureCursorVisible()
        #self.optimizeButton.setText(_("Run Optimizer"))
        self.readResultsCSV()
        self.readResultsYaml()
        self.stopOptimizer()
        
        # CSV Write Callback Functions (Could have used one version for all three instead of dupe code)
    def writeMinStatsCSV(self):
        for i in range(len(self.minStatsCSV)):
            for j in range(len(self.minStatsCSV[i])):
                if (self.minStatsWidgets[i][j].text() == ''):
                    self.minStatsCSV[i][j] = "0"
                else:
                    self.minStatsCSV[i][j] = self.minStatsWidgets[i][j].text()
        filepath = os.path.join(self.optimizerDir + '/' + 'min_stats.csv')
        self.writeCSV(filepath,self.minStatsCSV)
        
    def writeMaxStatsCSV(self):
        for i in range(len(self.maxStatsCSV)):
            for j in range(len(self.minStatsCSV[i])):
                if (self.maxStatsWidgets[i][j].text() == ''):
                    self.maxStatsCSV[i][j] = "0"
                else:
                    self.maxStatsCSV[i][j] = self.maxStatsWidgets[i][j].text()
        filepath = os.path.join(self.optimizerDir + '/' + 'max_stats.csv')
        self.writeCSV(filepath,self.maxStatsCSV)
        
    def writeAddStatsCSV(self):
        for i in range(len(self.addStatsCSV)):
            for j in range(len(self.addStatsCSV[i])):
                if (self.addStatsWidgets[i][j].text() == ''):
                    self.addStatsCSV[i][j] = "0"
                else:
                    self.addStatsCSV[i][j] = self.addStatsWidgets[i][j].text()
        filepath = os.path.join(self.optimizerDir + '/' + 'add_stats.csv')
        self.writeCSV(filepath,self.addStatsCSV)
        
    def calculateOrder(self):
        tempOrderArray = []
        override = False
        labels = ['a','b','c','d']
        for i in range(MAX_TURNS-1):
            if (self.orderWidgets[4][i+2].text() != ''):
                override = True;
                tempOrderArray.append(self.orderWidgets[4][i+2].text())
        
        speedIndex = []
        for i in range(MAX_ENTITIES-1):
            if (self.teamDressRows[i][6].text() == ''):
                speedIndex.append(0)
            else:
                speedIndex.append(int(self.teamDressRows[i][6].text()))
        speedIndex = np.argsort(speedIndex).tolist()
        speedIndex.reverse()

        for i in range(MAX_ENTITIES-2):
            if not (override):
                tempOrderList = []
                tempOrderList.append(labels[speedIndex[i]] + '>' + labels[speedIndex[i+1]])
                tempOrderArray.append(tempOrderList)
            if (i == MAX_ENTITIES-3):
                self.orderWidgets[i+1][0].setText(labels[speedIndex[i+1]])
                self.orderWidgets[i+1][1].setText(self.teamDressRows[speedIndex[i+1]][0].currentText())
                self.orderWidgets[i+1][1].setPalette(self.widgetColors[speedIndex[i+1]])
            self.orderWidgets[i][0].setText(labels[speedIndex[i]])
            self.orderWidgets[i][1].setText(self.teamDressRows[speedIndex[i]][0].currentText())
            self.orderWidgets[i][1].setPalette(self.widgetColors[speedIndex[i]])
        return tempOrderArray
    
    def writeOrderTXT(self):
        tempOrderArray = self.calculateOrder()
            
        filepath = os.path.join(self.optimizerDir + '/' + 'order.txt')
        self.writeCSV(filepath,tempOrderArray)
    
    # Results Functions
    def readResultsCSV(self):
        filepath = os.path.join(self.optimizerDir + '/' + 'results.csv')
        self.resultsCSV = pd.read_csv(filepath)
        self.resultsCSV = self.resultsCSV.T.reset_index().values.T.tolist()
        for i in range(len(self.resultsWidgets)):
            for j in range(len(self.resultsWidgets[i])):
                self.resultsWidgets[i][j].setText(str(self.resultsCSV[i][j]))
    
    def readResultsYaml(self):
        filepath = os.path.join(self.optimizerDir + '/' + 'results.yaml')
        with open(filepath, encoding='utf8') as resultsYAML:
            results = resultsYAML.read()
            self.resultsTextEdit.clear()
            resultsTextCursor = self.resultsTextEdit.textCursor()
            resultsTextCursor.movePosition(resultsTextCursor.End)
            resultsTextCursor.insertText(results)
            self.resultsTextEdit.ensureCursorVisible()
    
    # Extras' Functions
    def speedLinkCallback(self):
        values = []
        for i in [2,4,8]:
            if is_float(self.speedLinkingWidgets[i].text()):
                values.append(int(self.speedLinkingWidgets[i].text()))
                if (i == 4):
                    values[1] = values[1]/100
        if (len(values) == 3) and (values[0]*values[1]*values[2] > 0):
            if (self.speedLinkingWidgets[6].isChecked()):
                minSpeed = math.ceil(values[0]-(values[1]*100000+values[2]*(3*values[0]-10))/(13*values[2]+10*math.ceil(10000/values[0])))
            else:
                minSpeed = math.ceil(values[0]-values[1]*10000/(values[2]+math.ceil(10000/values[0])))
            self.speedLinkingWidgets[10].setText(str(minSpeed))
            
    def debuffNumCallback(self):
        values = []
        for i in [2,4]:
            if is_float(self.debuffNumWidgets[i].text()):
                values.append(int(self.debuffNumWidgets[i].text()))
                if (i == 2):
                    values[0] = values[0]/100
        if (len(values) == 2):
            if (self.debuffNumWidgets[6].isChecked()):
                chance = 1/(1-(1-values[0]*0.85)**values[1])-1/(0.85*values[0]*values[1])
            else:
                chance = 1+((1-values[0]*0.85)**values[1]-1)/(0.85*values[0]*values[1])
            chance = chance*100
            self.debuffNumWidgets[8].setText(str(round(chance,3)) + '%')
            
    def turnOverrideCallback(self):
        lastConstraint = -1
        for i in range(len(self.orderWidgets[4])-2):
            if ((self.orderWidgets[4][i+2].text()) != '') and (not (self.orderWidgets[4][i+2].text()).isspace()):
                lastConstraint = i;
        for i in range(len(self.orderWidgets[4])-2):
            if (i > lastConstraint):
                self.orderWidgets[4][i+2].setText('')
                if (lastConstraint == i-1):
                    self.orderWidgets[4][i+2].setVisible(True)
                    self.orderWidgets[4][i+2].setEnabled(True)
                elif (lastConstraint == i-2):
                    self.orderWidgets[4][i+2].setVisible(True)
                    self.orderWidgets[4][i+2].setEnabled(False)
                else:
                    self.orderWidgets[4][i+2].setVisible(False)
                    self.orderWidgets[4][i+2].setEnabled(False)
            
    def turnConstraintCallback(self):
        gaugeValues = []
        gaugeGain = []
        turnConstraint = []
        for i in range(2):
            if (self.turnConstraintWidgets[i*7+6].text() == ''):
                gaugeValues.append(0.0)
            else:
                gaugeValues.append(float(self.turnConstraintWidgets[i*7+6].text())/100)
            gaugeGain.append(10000*(int(self.turnConstraintWidgets[i*7+4].currentText())-gaugeValues[i]))
        try:
            turnConstraint.append(int(gaugeGain[1]/math.gcd(int(math.ceil(gaugeGain[0])),int(math.ceil(gaugeGain[1])))))
            turnConstraint.append(int(gaugeGain[0]/math.gcd(int(math.ceil(gaugeGain[0])),int(math.ceil(gaugeGain[1])))))
        except:
            turnConstraint.append(0)
            turnConstraint.append(0)
        
        constraintString = str(turnConstraint[0]) + '*' + self.turnConstraintWidgets[2].currentText() + ' > ' + str(turnConstraint[1]) + '*' + self.turnConstraintWidgets[9].currentText()
        self.turnConstraintWidgets[i*7+9].setText(constraintString)
    
    def createDressTab(self):
        try: 
            self.dataDressLayout.removeWidget(self.dressTableWidget)
            sip.delete(self.dressTableWidget)
            self.dressTableWidget = None
        except:
            pass
        
        self.dressTableWidget = QTableView()
        dressTableLayout = QGridLayout()
        
        tableModel = TableModel(self.dressesCSV)
        self.dressTableWidget.setModel(tableModel)
        
        self.dressTableWidget.setLayout(dressTableLayout)
        self.dataDressLayout.addWidget(self.dressTableWidget,1,0,1,2)
        
    # def createOrbTab(self):
        # dataOrbLayout = QGridLayout()
        # self.dataOrbTab.setLayout(dataOrbLayout)
        
    # def createDBTab(self):
        # dataDBLayout = QGridLayout()
        # self.dataDBTab.setLayout(dataDBLayout)
        
    # def createSkillModsTab(self):
        # dataSkillModsLayout = QGridLayout()
        # self.dataSkillModsTab.setLayout(dataSkillModsLayout)
    
    # Miscellaneous Functions
    def refreshLabels(self):
        # Main Tabs
        for i in range(len(self.labelStringArray[0])):
            self.MainTabWidget.setTabText(i, _(self.labelStringArray[0][i]))
        
        # Team + Turns
        for i in range(len(self.labelStringArray[1])):
            try:
                self.teamDressLabelObjects[i].setCurrentText(self.labelStringArray[1][i])
            except:
                self.teamDressLabelObjects[i].setText(self.labelStringArray[1][i])
        
        # Turn Simulator
        counter = 0
        for i in range(len(self.turnSimRows[0])):
            if (self.turnSimRows[0][i] != None):
                try:
                    self.turnSimRows[0][i].setCurrentText(self.labelStringArray[2][counter])
                except:
                    self.turnSimRows[0][i].setText(self.labelStringArray[2][counter])
                counter = counter + 1
            
        # ComboBoxes
            # Maybe we want to save the current selection and index back to it after?
        for i in range(MAX_ENTITIES-1):
            self.teamDressRows[i][0].clear()
            self.teamDressRows[i][0].addItems(self.dressList.iloc[:,currentLanguage].tolist())
            self.teamDressRows[i][0].setCurrentText("")
            
    def errorMsgPopup(self, details):
        errorBox = QMessageBox()
        errorBox.setIcon(QMessageBox.Critical)
        errorBox.setText(_("Error"))
        errorBox.setInformativeText(details)
        errorBox.setWindowIcon(QIcon('nya.png'))
        errorBox.setWindowTitle(_("MGCM Optimizer Error"))
        errorBox.exec_()
        
    

if __name__ == '__main__':
    setLanguage()
    os.environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    os.environ["QT_ENABLE_HIGHDPI_SCALING"] = "1"
    QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    gallery = WidgetGallery()
    gallery.show()
    sys.exit(app.exec_()) 