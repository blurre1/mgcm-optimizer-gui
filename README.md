# MGCM Optimizer GUI

Python GUI wrapper for MGCM Optimizer by Aeodyn.

https://gitlab.com/Aeodyn/mgcm-optimizer/-/tree/master

## Setup

Download the python script main.py and the locale folder and place them either in the same or parent directory of the MGCM-Optimizer. 

Download a copy of the Mults Calculator sheet as an '.xlsx' file and place it in the same directory. 

To run type the following:
python main.py

Or alternatively, if you have the .exe you can run that instead.


## Copyright and license

MGCM Optimizer GUI
Copyright (C) 2021  Blurre

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
